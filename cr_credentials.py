import os
import logging
import requests
from cr_member_settings import set_publisher_settings
import cr_settings
from getpass import getpass

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger()


def get_permitted_roles(username, password):
    try:
        response = requests.post(
            url=f"{cr_settings.cr_admin_base_uri}/servlet/login",
            data={
                "usr": username,
                "pwd": password,
            },
            headers=cr_settings.cr_user_agent
        )
        return response.json().get("roles", [])
    except requests.exceptions.RequestException as e:
        logger.error("Could not get roles: HTTP Request failed")
        raise e

def credential(username, password, role):
    return {
            "usr": username,
            "pwd": password,
            "role": role,
            "username": username,
            "password": password,
    }
    

def get_credentials_from_env():
    username = os.environ.get("CR_ADMIN_USERNAME")
    password = os.environ.get("CR_ADMIN_PASSWORD")
    role = os.environ.get("CR_ADMIN_ROLE")
    return credential(username, password, role) if (username and password and role) else None

def get_credentials_from_cli():
    username = input("Username: ")
    password = getpass("Password: ")
    
    if not username or not password:
        raise Exception("You must provide a username and password")
    permitted_roles = get_permitted_roles(username, password)
    if len(permitted_roles) == 0:
        raise Exception(
            f"You do not have any roles. Are your credentials correct for {cr_settings.cr_admin_base_uri}? "
        )
    print("Input the number of the role you wish to use")
    for count, role in enumerate(permitted_roles):
        print(f"{count} - {role}")
    role_number = input("Role number: ")
    role_number = int(role_number)
    role = permitted_roles[role_number]
    return credential(username, password, role)

def get_credentials():
    credentials = get_credentials_from_env()
    if not credentials:
        credentials = get_credentials_from_cli()
    return credentials
