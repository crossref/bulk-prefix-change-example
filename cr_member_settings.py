import sys
import logging
import requests
from bs4 import BeautifulSoup
from memoization import cached
from enum import Enum

import cr_settings

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger()


INPUTS_TO_SCRAPE = [
    "publisherID",
    "doi",
    "doi_data",
    "desc",
    "customerID",
    "handleUsername",
    "handlePassword",
    "bannerText",
]

REQUIRED_FIELDS = INPUTS_TO_SCRAPE + [
    "allowsOpenAccessToReferences",
    "modify",
    "imgFile",
]


class Availability(Enum):
    CLOSED = 0
    OPEN = 1
    LIMITED = 2


@cached()
def get_publisherUserAdmin_search_page(prefix: str, credentials: dict) -> str:
    url = f"{cr_settings.cr_admin_base_uri}/servlet/publisherUserAdmin?sf=search&doi={prefix}&start=0&size=20"
    headers = {"Content-Type": "application/x-www-form-urlencoded"} | cr_settings.cr_user_agent
    response = requests.request("POST", url, headers=headers, data=credentials)
    # We don't return correct HTTP status codes, need to scrape to see if we had
    # permission to do what we just tried to do.
    if "<login-screen/>" in response.text:
        raise Exception("Permisson to access admin page denied. Is your role correct?")
    return response.text


def scrape_system_id(html) -> int:

    soup = BeautifulSoup(html, "lxml")
    table_data_els = soup.find_all("td", class_="bgWhite")
    return table_data_els[1].text.strip() if len(table_data_els) > 1 else None


def get_system_id(prefix: str, credentials: dict) -> int:
    return scrape_system_id(get_publisherUserAdmin_search_page(prefix, credentials))


def get_publisherUserAdmin_modify_page(system_id: int, credentials: dict) -> str:
    params = {
        "sf": "modify",
        "publisherID": f"{system_id}",
    } | credentials
    url = f"{cr_settings.cr_admin_base_uri}/servlet/publisherUserAdmin"
    response = requests.request("get", url, headers=cr_settings.cr_user_agent, params=params)
    return response.text


def scrape_publisher_settings(html: str) -> dict:
    soup = BeautifulSoup(html, "lxml")
    inputs = soup.find_all("input")
    data = {
        input["name"]: input["value"]
        for input in inputs
        if input.get("name", None) in INPUTS_TO_SCRAPE
    }
    # pick up some stragglers that are not defined as input elements
    data["allowsOpenAccessToReferences"] = soup.select_one('option[selected=""]')[
        "value"
    ]
    data["modify"] = "modify"
    data["imgFile"] = "Binary File (0 bytes)"
    return data


def get_publisher_settings(prefix: str, credentials: dict):
    system_id = get_system_id(prefix, credentials)
    return (
        scrape_publisher_settings(
            get_publisherUserAdmin_modify_page(system_id, credentials)
        )
        if system_id
        else None
    )


def validate_settings(settings: dict) -> bool:
    for field in REQUIRED_FIELDS:
        if field not in settings:
            logger.warn(f'setting missing required field: "{field}"- no change made')
            return False
    logger.debug("settings complete")
    return True


def set_publisher_settings(settings: dict, credentials: dict):
    if validate_settings(settings):
        logger.info("updating settings")
        try:
            response = requests.post(
                url=f"{cr_settings.cr_admin_base_uri}/servlet/publisherUserAdmin",
                params={
                    "sf": "modify",
                },
                
                headers=cr_settings.cr_user_agent,
                data= settings | credentials,
            )
            logger.info(f'Response HTTP Status Code: {response.status_code}')
            logger.debug('Response HTTP Response Body: {content}'.format(
                content=response.content))
        except requests.exceptions.RequestException as e:
            logger.error('HTTP Request failed')
            raise e
    else:
        raise Exception("Incomplete settings provided")

