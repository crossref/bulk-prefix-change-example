import argparse
import logging
import cr_settings
from cr_credentials import get_credentials
from cr_member_settings import (
    Availability,
    get_publisher_settings,
    set_publisher_settings,
)

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger()


def open_refs(prefix: str, credentials: dict):

    current_publisher_settings = get_publisher_settings(prefix, credentials)
    if current_publisher_settings:
        current_ref_setting = int(
            current_publisher_settings["allowsOpenAccessToReferences"]
        )
        logger.debug(
            f"current ref setting for prefix {prefix}: {Availability(current_ref_setting).name}"
        )
        if Availability(current_ref_setting) == Availability.OPEN:
            logger.info(f"references for prefix {prefix} already open- skipping")
        else:
            logger.debug(f"setting references for prefix {prefix} to open")
            current_publisher_settings[
                "allowsOpenAccessToReferences"
            ] = f"{Availability.OPEN.value}"
            set_publisher_settings(current_publisher_settings, credentials)

    else:
        logger.warning(f'cannot find settings for prefix "{prefix}"-  doing nothing')


def parse_args() -> argparse.Namespace:

    parser = argparse.ArgumentParser(
        description="given a file of prefixes, set the prefs for each prefix to open"
    )

    parser.add_argument(
        "-v", "--verbose", help="print verbose output to console", action="store_true"
    )

    parser.add_argument(
        "-f",
        "--file",
        help="line-delimited list of prefixes to set to open",
        type=argparse.FileType("r"),
        required=True,
    )

    parser.add_argument(
        "-p", "--production", help="apply to production system", action="store_true"
    )

    return parser.parse_args()


def main():

    credentials = get_credentials()
    for line in ARGS.file:
        prefix = line.strip()
        logger.info(f"processing prefix {prefix}")
        open_refs(prefix, credentials)


if __name__ == "__main__":

    ARGS = parse_args()

    if ARGS.verbose:
        logger.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.WARNING)

    if ARGS.production:
        cr_settings.cr_admin_base_uri = "https://doi.crossref.org"
        logger.warning("applying to production system")
    else:
        logger.info("applying to test system")

    main()
    logger.info("done")
